const AWS = require('aws-sdk');
// Set the Region
AWS.config.update({region: 'us-west-2'});
const ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

const succeed = function(callback, body) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 200, 'body': (body instanceof String ? body : JSON.stringify(body))});
}

const fail = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 400, 'body': (prefix ? prefix : '') + (body instanceof String ? body : JSON.stringify(body))});
}

const fault = function(callback, body, prefix) {
    callback(null, {'headers': {'access-control-allow-origin': '*'}, 'statusCode': 500, 'body': (prefix ? prefix : '') + (body instanceof String ? body : JSON.stringify(body))});
}

exports.handler = function(event, context, callback) {
    console.log('Handling lambda invocation...');

/*
* sample event object:
*
* 2020-08-15T17:22:39.934Z	3fa6a4af-18bb-497e-8f88-eaa6a18a062b	INFO	Event:
* {
*     "version": "2.0",
*     "routeKey": "$default",
*     "rawPath": "/Prod/isAlive",
*     "rawQueryString": "clientId=12321",
*     "headers": {
*         "content-length": "0",
*         "host": "diagnostics.pennidinh.com",
*         "user-agent": "curl/7.54.0",
*         "x-amzn-trace-id": "Root=1-5f3819df-c82134e3f8a2f1bb73b78fc7",
*         "x-forwarded-for": "73.167.111.89",
*         "x-forwarded-port": "443",
*         "x-forwarded-proto": "https"
*     },
*     "queryStringParameters": {
*         "clientId": "12321"
*     },
*     "requestContext": {
*         "accountId": "334099949173",
*         "apiId": "cnlkwt2wn3",
*         "domainName": "diagnostics.pennidinh.com",
*         "domainPrefix": "diagnostics",
*         "http": {
*             "method": "GET",
*             "path": "/Prod/isAlive",
*             "protocol": "HTTP/1.1",
*             "sourceIp": "73.167.111.89",
*             "userAgent": "curl/7.54.0"
*         },
*         "requestId": "RUj6_hzmPHcEMpw=",
*         "routeKey": "$default",
*         "stage": "Prod",
*         "time": "15/Aug/2020:17:22:39 +0000",
*         "timeEpoch": 1597512159890
*     },
*     "isBase64Encoded": false
* }
*
* Sample context object:
*
* {
*     "callbackWaitsForEmptyEventLoop": true,
*     "functionVersion": "$LATEST",
*     "functionName": "PenniDinhCentral-Diagnostics",
*     "memoryLimitInMB": "128",
*     "logGroupName": "/aws/lambda/PenniDinhCentral-Diagnostics",
*     "logStreamName": "2020/08/15/[$LATEST]6649d54877dd4eac8fafcd275977bf02",
*     "invokedFunctionArn": "arn:aws:lambda:us-west-2:334099949173:function:PenniDinhCentral-Diagnostics",
*     "awsRequestId": "3fa6a4af-18bb-497e-8f88-eaa6a18a062b"
* }
*/

    const path = event.rawPath;
    console.log('path: ' + path);
    const sourceIp = event.requestContext.http.sourceIp;

    if (path === '/Prod/') {
       const bodyString = Buffer.from(event.body, 'base64');
       console.log(sourceIp + ' body: ' + bodyString);
       const bodyJson = JSON.parse(bodyString);
       const clientId = bodyJson['clientId'];
       const publicKey = bodyJson['publicKey'];

       if (!clientId) {
         fail(callback, 'clientId in body cannot be empty!');
         return;
       }
       if (!publicKey) {
         fail(callback, 'publicKey in body cannot be empty!');
         return;
       }

       const params = {Key: {"ClientId": {S: clientId}}, TableName: process.env.UnclaimedClientIdsTableName};
       ddb.getItem(params, function(err, data) {
         if (err) {
           fault(callback, 'error while fetching from table: ' + err);
           return;
         }

         if (!data.Item) {
           fail(callback, 'clientId: ' + clientId + ' is not available to claim');
           return;
         }

         ddb.putItem({Item: {ClientId: {S: clientId}, PublicKey: {S: publicKey}}, TableName: process.env.ClientPublicKeysTableName}, function(err, data) {
           if (err) {
             fault(callback, 'error while putting to table: ' + err);
             return;
           }

           ddb.deleteItem(params, function(err, data) {
             if (err) {
               fault(callback, 'error while deleting from table: ' + err);
               return;
             }

             succeed(callback, 'successfully claimed: ' + clientId);
           });
         });
       });
    } else {
       fail(callback, 'Unknown API: ' + path);
    }
}
